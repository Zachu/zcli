zcli
====

zcli is a wrapper for gathering multiple cli tools together under one umbrella.

## Usage

### Installation

1. You might want to create a virtualenv for this first
   ```sh
   $ mkvirtualenv -p $(which python3) zcli
   ```

TODO

## TODO

- Document !include (https://pypi.org/project/pyyaml-include/#description)
- Document envvars
- Document module configs for module developers
- Test config passing
- Tools like read/write jsonl(.gz)
- List available modules
