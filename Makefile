test:
	coverage run --source tests -m unittest discover --verbose
	coverage report

pylint:
	pylint zcli

clean:
	rm -rf htmlcov .coverage .eggs *.egg-info
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
