import unittest

import zcli.utils


class TestUtils(unittest.TestCase):
    def test_deep_merge_simple_dicts(self):
        deep_merge = zcli.utils.deep_merge

        obj1 = {'foo': 'bar'}
        obj2 = {'baz': 'goa'}
        rslt = {'foo': 'bar', 'baz': 'goa'}
        self.assertEqual(deep_merge(obj1, obj2), rslt, msg='Simple dicts should be merged'),

    def test_deep_merge_deep_dicts(self):
        deep_merge = zcli.utils.deep_merge

        obj1 = {
            'lvl1': {
                '1-key-1': '1-value-1',
                '1-key-2': '1-value-2',
            },
            'lvl2': {
                '2-key-1': '2-value-1',
                '2-key-2': '2-value-2',
            },
        }
        obj2 = {
            'lvl1': {
                '1-key-3': '1-value-3',
            },
        }
        rslt = {
            'lvl1': {
                '1-key-1': '1-value-1',
                '1-key-2': '1-value-2',
                '1-key-3': '1-value-3',  # Should get added
            },
            'lvl2': {
                '2-key-1': '2-value-1',
                '2-key-2': '2-value-2',
            },
        }
        self.assertEqual(deep_merge(obj1, obj2), rslt, msg='Deep dicts should be merged'),

    def test_deep_merge_append_lists(self):
        deep_merge = zcli.utils.deep_merge
        obj1 = {'list': ['0', '1', '2']}
        obj2 = {'list': ['a', 'b', 'c']}
        self.assertEqual(
            deep_merge(obj1, obj2),
            {'list': ['a', 'b', 'c']},
            msg='By default lists should not be appended',
        )
        self.assertEqual(
            deep_merge(obj1, obj2, list_append=True),
            {'list': ['0', '1', '2', 'a', 'b', 'c']},
            msg='Lists should be appended',
        )


if __name__ == '__main__':
    unittest.main()
