from setuptools import find_packages, setup


def determine_version(root):
    import setuptools_scm
    version = setuptools_scm.version_from_scm(root)
    if version is None:
        try:
            version = setuptools_scm.get_version()
        except Exception:
            pass
    if version is None:
        try:
            with open('VERSION.txt', mode='r') as f:
                version = f.read().strip()
        except IOError as ex:
            print(ex)
            version = 'UNKNOWN'
    return version


def _get_install_config():
    import configparser
    import os
    cfg_file = os.path.join(
        os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~/.config')),
        'zcli', 'install.ini',
    )
    config = configparser.ConfigParser()
    config.read(cfg_file)
    return config

def _get_modules():
    import ast
    config = _get_install_config()
    return ast.literal_eval(config.get('main', 'modules', fallback='[]'))

def _get_module_requires():
    config = _get_install_config()
    requires = {}
    for module in _get_modules():
        requires[module] = config.get(module, 'source', fallback=module)
        if config.has_option(module, 'version') and '{version}' in requires[module]:
            requires[module] = requires[module].format(version=config.get(module, 'version'))
        elif config.has_option(module, 'version') and not config.has_option(module, 'source'):
            requires[module] += config.get(module, 'version')
    return requires

def _get_module_entrypoints():
    config = _get_install_config()
    entrypoints = []
    for module in _get_modules():
        entrypoints.append(
            '{module} = {entrypoint}'.format(
                module=module,
                entrypoint=config.get(module, 'entrypoint', fallback='{}.__main__:cli'.format(module))
            )
        )
    return entrypoints

tests_require = [
   'coverage>5,<6',
   'pylint>2,<3',
]

install_requires=[
    'click>=8',
    'pyyaml>=5,<6',
    'pyyaml-include>=1.2',
]
install_requires.extend(_get_module_requires().values())

setup(
    name='zcli',
    use_scm_version=True,
    packages=find_packages(),

    url='https://gitlab.com/Zachu/zcli',
    author='Jani Korhonen',
    author_email='jani@zachu.fi',
    description='Wrapper for multiple cli tools.',

    setup_requires=['setuptools_scm'],
    install_requires=install_requires,
    tests_require=tests_require,
    test_suite='tests',

    extras_require={
        'tests': tests_require,
    },

    entry_points={
        'console_scripts': ['zcli = zcli.cli.__main__:main'],
        'modules': _get_module_entrypoints(),
    },
)
