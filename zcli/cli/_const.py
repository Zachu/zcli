CONTEXT_SETTINGS = {
    'auto_envvar_prefix': 'ZCLI',
}

DEFAULT_CONFIG = {
    'default_stdout': None,  # None for stdout, False for /dev/null, string for filename
    'default_stderr': None,  # None for stderr, False for /dev/null, string for filename
}
