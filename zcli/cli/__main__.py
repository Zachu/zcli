"""Main file for zcli"""
import logging
import os
import sys

import click
import yaml

from ._const import DEFAULT_CONFIG, CONTEXT_SETTINGS
from zcli import CONFIG_PATH
from zcli.utils import echo, gzip_support, deep_merge


# TODO: Make configurable
logging.basicConfig(stream=sys.stderr)


def _read_config(file=None):
    if not file:
        try:
            file = gzip_support(click.open_file(CONFIG_PATH, mode='r'))
        except FileNotFoundError:
            pass
    else:
        file = gzip_support(file)

    file_path = os.path.abspath(file.name) if file is not None else None

    config = {'main': DEFAULT_CONFIG}
    for module_name, module in _get_modules():
        module_cfg = getattr(sys.modules[module._module_name], 'DEFAULT_CONFIG', {})
        if module_cfg:
            config[module_name] = module_cfg

    if file:
        try:
            for data in yaml.load_all(file, Loader=yaml.SafeLoader):
                config = deep_merge(config, data)
        except RecursionError as exc:
            echo("Error: {} while reading configuration from {}".format(str(exc), file_path), err=True)
            sys.exit(1)

    return file_path, config


def _get_modules():
    return [(name, cmd) for name, cmd in main.commands.items() if hasattr(cmd, '_module_name')]


def _parse_default_output(name):
    ctx = click.get_current_context()
    cfg_output = ctx.meta['config'].get('main', {}).get('default_' + name, None)
    output = None
    if cfg_output is False:
        output = open(os.devnull, 'w')
    elif cfg_output:
        output = open(cfg_output, 'w')
    return output


@click.group(context_settings=CONTEXT_SETTINGS)
@click.option('--config', '-c', type=click.File('r'), help='Path to config file.')
@click.option('--stdout', '-o', type=click.File(mode='w'), help='Path to default output. Defaults to stdout.')
@click.option('--stderr', '-e', type=click.File(mode='w'), help='Path to default error output. Defaults to stderr.')
@click.option('--profile', '-p', is_flag=True, help='Output cProfile stats to stderr.')
def main(config=None, stdout=None, stderr=None, profile=False):
    """Wrapper for bunch of cli tools."""
    if profile:
        # Initial code from https://stackoverflow.com/a/60474929/3173125
        # pylint: disable=import-outside-toplevel  # Import profiling libraries only when needed
        import cProfile
        import pstats
        import io
        import atexit

        click.echo('Profiling...', err=True)
        pro = cProfile.Profile()
        pro.enable()

        def pr_exit():
            pro.disable()
            click.echo('Profiling completed', err=True)
            str_stream = io.StringIO()
            pstats.Stats(pro, stream=str_stream).sort_stats('cumulative').print_stats()
            click.echo(str_stream.getvalue(), err=True)
        atexit.register(pr_exit)

    ctx = click.get_current_context()

    # Parse configuration
    ctx.meta['config_path'], ctx.meta['config'] = _read_config(config)

    # Set default stdout and stderr
    ctx.meta['default'] = {
        'stdout': gzip_support(stdout if stdout else _parse_default_output('stdout')),
        'stderr': gzip_support(stderr if stderr else _parse_default_output('stderr')),
    }


@main.command()
@click.option('--file', '-f', type=click.File(mode='r'))
def show_config(file=None):
    """Show current effective configuration or read configuration from file"""
    if file:
        file_path, config = _read_config(file)
    else:
        ctx = click.get_current_context()
        file_path = ctx.meta['config_path'] if ctx.meta['config_path'] is not None else f'{CONFIG_PATH} missing. Showing default configuration'
        config = ctx.meta['config']
    echo('# ' + file_path)
    echo('---')
    echo(yaml.dump(config))


@main.command()
@click.argument('module')
def extract_config(module):
    """Extract default config from module"""
    ctx = click.get_current_context()
    if module == 'main':
        module_name = __name__
    else:
        module_name = getattr(main.get_command(ctx, module), '_module_name', None)

    if not module_name or module_name not in sys.modules:
        echo('Module {} is not loaded'.format(module), err=True)
        sys.exit(1)

    config = getattr(sys.modules[module_name], 'DEFAULT_CONFIG', {})  # TODO: Document this

    echo('---')
    echo(yaml.dump(config))


if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter
