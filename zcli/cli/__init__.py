"""Init for zcli"""
import pkg_resources
import os

from .__main__ import main
import zcli.utils.echo


for entry_point in pkg_resources.iter_entry_points('modules'):
    try:
        module = entry_point.load()
        setattr(module, '_module_name', entry_point.module_name)
        main.add_command(module, name=entry_point.name)
    except ModuleNotFoundError as exc:
        # TODO: Give warnings if on verbose/debug mode
        zcli.utils.echo('WARN: ' + str(exc), err=True)
