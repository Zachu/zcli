"""Init for zcli"""
import os

import yaml
from yamlinclude import YamlIncludeConstructor


CONFIG_PATH = os.path.join(
    os.environ.get('XDG_CONFIG_HOME', os.path.expanduser('~/.config')),
    __package__,
    'config.yaml',
)


# Yaml !include statements
YamlIncludeConstructor.add_to_loader_class(loader_class=yaml.SafeLoader, base_dir=os.path.dirname(CONFIG_PATH))
