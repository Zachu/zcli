import inspect


def get_signature(func: callable, *args, **kwargs) -> str:
    bind = inspect.signature(func).bind(*args, **kwargs)
    bind.apply_defaults()
    argstr = ', '.join([f'{arg[0]}={arg[1]}' for arg in bind.arguments.items()])
    return f'{func.__module__}.{func.__name__}({argstr})'
