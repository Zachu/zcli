import datetime
from functools import wraps
import hashlib
import json
import logging
import os
import pickle
import time

from zcli.utils import get_signature, gzip_support


CACHE_DIR = os.path.join(os.environ.get('XDG_CACHE_HOME', os.path.expanduser('~/.cache')), 'zcli')
LOG = logging.getLogger(__name__)
BINARY_SERIALIZERS = {'pickle'}
# LOG.addHandler(logging.NullHandler())

import sys
logging.basicConfig(stream=sys.stderr)


def cache_disk(expire: int, path_prefix: str = '', filename: str = None, gzip=False, serializer='json'):
    def get_cachefile(filename: str, signature: str) -> str:
        if not filename:
            return os.path.join(CACHE_DIR, path_prefix, hashlib.sha256(signature.encode()).hexdigest())
        return os.path.join(CACHE_DIR, path_prefix, filename)

    def is_expired(filename: str, expire: int) -> bool:
        try:
            mtime = datetime.datetime.fromtimestamp(os.path.getmtime(filename))
            if mtime + datetime.timedelta(seconds=expire) > datetime.datetime.now():
                return False
        except FileNotFoundError:
            pass
        except Exception as exc:  # pylint: disable=broad-except  # Intentionally logging all exceptions
            LOG.warn('Cache reading failed: Unable to read mtime from %s. %s', filename, exc)
        return True

    def read_cache(filename: str):
        with open(filename, 'rb' if serializer in BINARY_SERIALIZERS else 'rt') as f:
            if gzip:
                try:
                    f = gzip_support(f, force=True)
                except Exception as exc:
                    pass

            if serializer == 'json':
                return json.load(f)
            elif serializer == 'pickle':
                return pickle.load(f)

    def write_cache(filename: str, data) -> None:
        dirname = os.path.dirname(filename)
        os.makedirs(dirname, exist_ok=True)
        with open(filename, 'wb' if serializer in BINARY_SERIALIZERS else 'wt') as f:
            if gzip:
                f = gzip_support(f, force=True)

            if serializer == 'json':
                json.dump(data, f)
            elif serializer == 'pickle':
                pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)

    def run_and_cache(func: callable, filename: str, *args, **kwargs):
        call_start = time.perf_counter()
        result = func(*args, **kwargs)
        call_end = time.perf_counter()
        try:
            write_cache(filename, result)
        except Exception as exc:  # pylint: disable=broad-except  # Intentionally logging all exceptions
            LOG.warn('Unable to write cache to %s. %s', filename, exc)
        write_end = time.perf_counter()
        return (
            call_end-call_start,  # Call duration
            write_end-call_end,  # Cache write duration
            result,
        )

    def decorator(func: callable):
        @wraps(func)
        def wrap(*args, **kwargs):
            sig = get_signature(func, *args, **kwargs)
            cachefile = get_cachefile(filename=filename, signature=sig)
            cached = False
            if not is_expired(cachefile, expire):
                try:
                    result = read_cache(cachefile)
                    cached = True
                except Exception as exc:  # pylint: disable=broad-except  # Intentionally logging all exceptions
                    LOG.warn('Cache reading failed from file %s. %s', cachefile, exc)

            if not cached:
                call_time, caching_time, result = run_and_cache(func, cachefile, *args, **kwargs)
                LOG.info('Cache miss for %s. Refreshed cache in %.3fs.', func.__name__, call_time+caching_time)
                LOG.debug('Cache miss for %s. call_time=%s, caching_time=%s', sig, call_time, caching_time)

            return result
        return wrap
    return decorator
