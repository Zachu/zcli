import copy


def deep_merge(*args, list_append=False):
    """Deep merge number of dicts together.

    Sources are copied with `copy.deepcopy`. A dict given later overrides previous dict values.
    """

    def merge(orig, new, list_append=list_append):
        if not new:
            return orig

        for key, value in new.items():
            if isinstance(value, dict) and value:
                orig.setdefault(key, {})
                orig[key] = merge(orig.get(key), value)
            elif isinstance(value, list) and isinstance(orig.get(key, None), list) and list_append:
                orig[key].extend(value)
            else:
                orig[key] = value

        return orig

    result = {}
    for arg in args:
        result = merge(result, copy.deepcopy(arg), list_append=list_append)

    return result
