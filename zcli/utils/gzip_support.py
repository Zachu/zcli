import gzip
import os


def gzip_support(file, force=False):
    """Change file handle to gzip if needed"""
    if not file:
        return None

    _, ext = os.path.splitext(file.name)
    if force or ext == '.gz':
        file.close()
        mode = file.mode
        if 'b' not in mode and 't' not in mode:  # Default to text mode as open() does too.
            mode += 't'
        file = gzip.open(file.name, mode=mode)
    return file
