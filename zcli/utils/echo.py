import click


def echo(*args, **kwargs):
    """Echo but configurable default outputs"""
    try:
        ctx = click.get_current_context()
        if 'file' not in kwargs:
            if kwargs.get('err', False):
                kwargs['file'] = ctx.meta.get('default', {}).get('stderr', None)
            else:
                kwargs['file'] = ctx.meta.get('default', {}).get('stdout', None)
    except RuntimeError:
        # RuntimeError: There is no active click context.
        pass

    click.echo(*args, **kwargs)
