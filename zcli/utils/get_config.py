import click


def get_config(module, defaults=None, key=None):
    ctx = click.get_current_context()
    if not defaults:
        defaults = {}

    cfg = {
        **defaults,
        **ctx.meta.get('config', {}).get(module, {})
    }
    if key:
        return cfg.get(key, None)
    return cfg
